import java.util.Random;
import java.util.Scanner;

public class GuessTheNumber {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        System.out.println("Hello! what you name?");
        String name = sca.nextLine();
        boolean KeepPlaying = true;

        Random rand = new Random();
        int randValue = 1 + rand.nextInt(20);
        int count;
        while (KeepPlaying){
            count = 1;
            System.out.println("Well," + name+", I am thinking of a number between 1 and 20");
            System.out.println("Take a guss");
            int username = sca.nextInt();
            while (username!= randValue){
                count++;
                if(username > randValue){
                    System.out.println("Your guess is too high");
                }
                else{
                    System.out.println("Your guess is too low");
                }
                System.out.println("Take a guess");
                username = sca.nextInt();
            }
            System.out.println("Good job," + name+"!You guessed my number in" + count+ "guesses!");
            System.out.println("Would you like to play again? (Y or N)");
            String playResponse = sca.next();
            KeepPlaying = !(playResponse.startsWith("N")||playResponse.startsWith("n"));
        }
    }
}
